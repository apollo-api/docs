NODE=16

network:
	./network.sh

compile: install
	docker run --user node -i --rm --name compile-sauce-docs \
	-e NODE_ENV=production \
	-e PUBLIC_URL="/sauce-docs-ui-static" \
	-v `pwd`:/usr/src/app \
	-w /usr/src/app node:${NODE} npm run-script build

compile-generator: install
	docker run --user node -i --rm --name compile-sauce-docs -e NODE_ENV=production \
	-v `pwd`:/usr/src/app -w /usr/src/app node:${NODE} npm run-script build-ts

install:
	docker run -i --rm --name install-sauce-docs -v `pwd`:/usr/src/app -w /usr/src/app node:${NODE} npm install ${PCKG}

install-dev:
	docker run -i --rm --name install-sauce-docs -v `pwd`:/usr/src/app -w /usr/src/app node:${NODE} npm install ${PCKG} --save-dev

down:
	docker-compose down

up:
	docker-compose up

run: down install up

package:
	/bin/sh ./bin/package.sh

publish-ci: install
	docker run -i --rm -p "9198:1337" \
	-v `pwd`:/usr/src/app -w /usr/src/app \
	-e NPM_TOKEN=$(NPM_TOKEN) \
	node:${NODE} bin/publish-ci.js