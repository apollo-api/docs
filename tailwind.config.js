module.exports = {
    content: [
        "./src/**/*.{js,ts,jsx,tsx}",
        "./node_modules/daisy-ui-react-components/**/*.{js,ts,jsx,tsx}"
    ],
    plugins: [require("@tailwindcss/typography"), require("daisyui")],
    daisyui: {
        themes: [
            {
                mytheme: {
                    "primary": "#DB2B11",
                    "secondary": "#96A9A2",
                    "accent": "#96140C",
                    "neutral": "#1A1320",
                    "base-100": "#F5E9D6",
                    "info": "#76DBF4",
                    "success": "#46D8B3",
                    "warning": "#F9BF62",
                    "error": "#E95D58",
                },
            },
        ],
    }
};

