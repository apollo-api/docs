import React from "react";
import { v4 as uuidv4 } from "uuid";
import clsx from "clsx";

export interface PropTableItem {
    name :string,
    description :string
    dataType :string
    required ?:boolean
}
interface ITable {
    headerClasses ?:string
    className ?:string
    properties :PropTableItem[]
}
export function Table({headerClasses, className, properties} :ITable) {
    return (
        <table className={`table-compact w-full ${className || ""}`}>
            <thead>
                <tr>
                    { ["Name", "Description", "Datatype", "Required"].map(field =>
                        <th key={uuidv4()} className={`${headerClasses || ""}`}>
                            {field}
                        </th>
                    )}
                </tr>
            </thead>

            <tbody>
                {
                    properties.map(prop =>(
                        <tr key={uuidv4()}>
                            <td><span className="font-mono font-bold">{prop.name}</span></td>
                            <td dangerouslySetInnerHTML={{__html: prop.description}}></td>
                            <td><span className="badge badge-ghost">{prop.dataType}</span></td>
                            <td>
                                <span className={clsx(
                                    "badge",
                                    prop.required && "badge badge-primary"
                                )}>
                                    {prop.required !== undefined ? prop.required.toString() : "Default"}
                                </span>
                            </td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    );
};
