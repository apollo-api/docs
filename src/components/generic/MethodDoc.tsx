import React, { PropsWithChildren } from "react";
import { PropTableItem, Table } from "./Table";
import { Light as SyntaxHighlighter } from "react-syntax-highlighter";
import ts from "react-syntax-highlighter/dist/esm/languages/hljs/typescript";
import atom from "react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark";
import { Collapse, CollapseBody, CollapseTitle } from "daisy-ui-react-components";
SyntaxHighlighter.registerLanguage("typescript", ts);

interface IMethodDoc {
    header :string
    methodArgs ?:PropTableItem[]
    usageExample ?:string
    returnsExample ?:string

}
export const MethodDoc = ({header, methodArgs, usageExample, returnsExample, children} :PropsWithChildren<IMethodDoc>)=>{
    return (
        <>
            <h2 className="mb-3 pb-2 border-b">{header}</h2>
            {children}
            <div className="overflow-x-auto">
                {
                    methodArgs && <Table
                        className="mt-1 mb-0"
                        properties={methodArgs}
                    />
                }
            </div>

            {
                usageExample &&
                <Collapse defaultExpand>
                    <CollapseTitle className="lg:w-1/6 sm:w-1/4">
                        <span className="w-full font-bold p-0">Usage</span>
                    </CollapseTitle>

                    <CollapseBody>
                        <SyntaxHighlighter
                            language="typescript"
                            className="bg-neutral text-neutral-content m-2 p-3 rounded-md overflow-x-auto"
                            style={atom}
                        >
                            {usageExample}
                        </SyntaxHighlighter>
                    </CollapseBody>
                </Collapse>
            }

            {
                returnsExample &&
                <Collapse defaultExpand>
                    <CollapseTitle className="lg:w-1/6 sm:w-1/4">
                        <span className="w-full font-bold p-0">Returns</span>
                    </CollapseTitle>

                    <CollapseBody>
                        <SyntaxHighlighter
                            language="typescript"
                            className="bg-neutral text-neutral-content m-2 p-3 rounded-md overflow-x-auto"
                            style={atom}
                        >
                            {returnsExample}
                        </SyntaxHighlighter>
                    </CollapseBody>
                </Collapse>
            }
        </>
    );
};
