import { MenuItem } from "daisy-ui-react-components";
import React from "react";
import { Link, LinkProps, useMatch, useResolvedPath } from "react-router-dom";

interface IMenuLink {
    isDisabled ?:boolean
    onClick ?:(e :React.MouseEvent<HTMLLIElement, MouseEvent>) => any
}
export function MenuLink({ children, to, onClick, isDisabled, ...props }: Omit<LinkProps, "onClick"> & IMenuLink) {
    const resolved = useResolvedPath(to);
    const match = useMatch({ path: resolved.pathname, end: true });

    return (
        <MenuItem
            isBordered={!!match}
            onClick={onClick ? e => onClick(e) : undefined}
            isDisabled={isDisabled}
        >
            {
                isDisabled ? <span className="gap-x-0">{children}</span> :
                    <Link to={`${to}`} {...props} className="gap-x-0">{children}</Link>
            }
        </MenuItem>
    );
}
