import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "daisy-ui-react-components";
import React, { PropsWithChildren, useRef } from "react";

interface ICode{
    disableCopy ?:boolean
    className ?:string
    language ?:string
}
export function Code({children, disableCopy, className, language}:PropsWithChildren<ICode>) {
    const codeRef = useRef<HTMLPreElement>(null);
    const copy = async ()=>{
        try{
            if(codeRef.current) {
                await navigator.clipboard.writeText(codeRef.current.innerText);
            }
        }
        catch(err) {
            console.error(err);
        }
    };

    return (
        <div className={`inline-flex items-center ${className || ""} ${language}`}>
            <pre ref={codeRef} className={`${!disableCopy ? "rounded-l-md rounded-r-none" : ""} m-0 whitespace-pre-line w-full`}>
                <code className={`${language || ""}`}>
                    {children}
                </code>
            </pre>
            {
                !disableCopy &&
                <Button variant="primary" className="gap-2 rounded-l-none rounded-r-md" onClick={copy}>
                    <FontAwesomeIcon icon={"clipboard"} />
                </Button>
            }
        </div>
    );
}
