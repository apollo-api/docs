export default function() {
    return (
        <footer className="footer items-center mt-4 p-4 bg-base-100 text-base-content">
            <div className="items-center grid-flow-col">
                <p>Copyright © {new Date().getFullYear()} - All right reserved</p>
            </div>
        </footer>
    );
}
