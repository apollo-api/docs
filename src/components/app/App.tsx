import React from "react";
import { Route, Routes } from "react-router-dom";
import { Home } from "../pages/home/Home";
import {Navbar} from "../navbar/Navbar";
import { Installation } from "../pages/installation/Installation";
import { Starting } from "../pages/starting/Starting";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";
import { SauceConfig } from "../pages/sauce-config/SauceConfig";
import { NotFound } from "../pages/not-found/NotFound";
import { SauceObject } from "../pages/sauce-object/SauceObject";
import { Controllers } from "../pages/controllers/Controllers";
import { Policies } from "../pages/policies/Policies";
import { Responses } from "../pages/responses/Responses";
import Footer from "../footer/Footer";
import { Button } from "daisy-ui-react-components";
import { RouteParams } from "../pages/route-params/RouteParams";
import { VisualizerUI } from "../pages/plugins/visualizer-ui/VisualizerUI";

function App() {
    document.documentElement.setAttribute("data-theme", "winter");

    return (
        <>
            <div className="drawer drawer-mobile">
                <input id="main-drawer" type="checkbox" className="drawer-toggle" />
                <div className="drawer-content">

                    <div className="navbar bg-base-100">
                        <div className="flex-none">
                            <label htmlFor="main-drawer" className="btn btn-square btn-ghost drawer-button lg:hidden">
                                <FontAwesomeIcon icon={"hamburger"} size={"2x"}/>
                            </label>
                        </div>
                        <div className="flex-1 ml-2">
                            <div className="logo-font font-title text-primary inline-flex text-3xl transition-all duration-200 lg:hidden">
                                <span className="text-base-content mr-2">Sauce</span>
                                <span>Api</span>
                            </div>
                        </div>

                        <div className="flex-none">
                            <a href="https://gitlab.com/sauce-api" target="_blank">
                                <Button className="btn-ghost btn-square">
                                    <FontAwesomeIcon icon={faGitlab} size={"2x"}/>
                                </Button>
                            </a>
                        </div>
                    </div>

                    <Routes>
                        <Route path={"/"} element={<Home />} />
                        <Route path={"/installation"} element={<Installation />} />
                        <Route path={"/starting-up"} element={<Starting />} />
                        <Route path={"/sauce-config"} element={<SauceConfig />} />
                        <Route path={"/sauce-object"} element={<SauceObject />} />
                        <Route path={"/controllers"} element={<Controllers />} />
                        <Route path={"/policies"} element={<Policies />} />
                        <Route path={"/responses"} element={<Responses />} />
                        <Route path={"/route-params"} element={<RouteParams />} />
                        <Route path={"/visualizer-ui"} element={<VisualizerUI />} />
                        <Route path="*" element={<NotFound />} />
                    </Routes>
                    <Footer />
                </div>
                <div className="drawer-side">
                    <label htmlFor="main-drawer" className="drawer-overlay"></label>
                    <Navbar />
                </div>
            </div>
        </>
    );
}

export default App;
