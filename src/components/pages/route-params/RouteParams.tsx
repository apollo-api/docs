// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Link } from "react-router-dom";
import { Light as SyntaxHighlighter } from "react-syntax-highlighter";
import ts from "react-syntax-highlighter/dist/esm/languages/hljs/typescript";
import atom from "react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark";
SyntaxHighlighter.registerLanguage("typescript", ts);
import { PropTableItem, Table } from "../../generic/Table";
import Examples from "./Examples";

export const RouteParams = () => {
    const configProps :PropTableItem[] = [
        {
            name: "name",
            description: "A string representing the name of the route parameter",
            dataType: "string"
        },
        {
            name: "children",
            description: "An array of RouteParam objects representing child parameters of the current route parameter",
            dataType: "array<RouteParam>"
        },
        {
            name: "required",
            description: "A boolean value indicating whether the route parameter is required or optional",
            dataType: "boolean"
        },
        {
            name: "type",
            description: "A string representing the data type of the route parameter. It can be one of the following: \"boolean\", \"number\", \"string\", \"object\", \"array\", or \"enum\"",
            dataType: "enum"
        },
        {
            name: "enumValues",
            description: "An array of string, number or boolean values representing the allowed values for an enum type route parameter",
            dataType: "array<string|number|boolean>"
        },
        {
            name: "description",
            description: "A string representing the description of the route parameter",
            dataType: "string"
        },
        {
            name: "customValidator",
            description: "A custom validator function that can be used to validate this route param",
            dataType: "(reqParamValue, req)=>void"
        },
    ];

    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose max-w-4xl flex-grow">
                <h1>Route Params</h1>
                <p>
                    The RouteParam class is a component that defines a route parameter for the Sauce framework to pick up.
                    A route parameter can be a parameter in the path, body, or a query parameter.
                </p>

                <p>See <Link to={"/routes"} className="text-sm">Routes</Link> for how Route Params are used by Routes</p>

                <h2 className="mb-0">Properties</h2>
                <div className="overflow-x-auto">
                    <Table
                        properties={configProps}/>
                </div>
            </div>

            <p>Example Usage: </p>
            <div className="prose w-full max-w-4xl flex-grow">
                <SyntaxHighlighter
                    language="typescript"
                    className="bg-neutral text-neutral-content m-2 p-3 rounded-md overflow-x-auto"
                    style={atom}
                >
                    {Examples}
                </SyntaxHighlighter>
            </div>
        </div>
    );
};
