export default
`import { formatError, RouteParam } from "@sauce-api/core";
import { Request } from "express";

new RouteParam()
    .setName("id")
    .setType("number")
    .setRequired(true)
    .setDescription("The ID of the item");

new RouteParam()
    .setName("startDate")
    .setType("string")
    .setDescription("ISO string of the start date of this thing")
    .setRequired(true)
    .setCustomValidator(isIsoDateParam)

// This will enforce that the incoming string for the route param is indeed foramtted as an ISO string
const isIsoDateParam = (requestParamValue :any, req :Request)=>{
    const errorRes = formatError(400, "Date string must be in ISO8601:2000 format");
    if (!/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/.test(requestParamValue)) {
        throw errorRes;
    };
    const d = new Date(requestParamValue);
    const isValid = Date && !isNaN(d as any) && d.toISOString()===requestParamValue; // valid date
    if(!isValid) {
        throw errorRes;
    }
};
`;
