import React from "react";
import { Code } from "../../../generic/Code";

export const VisualizerUI = () => {
    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose w-full max-w-4xl flex-grow">
                <h1>Visualizer UI</h1>
                <p>A Swagger UI esque visualizer for Sauce API</p>

                <h1>Install</h1>
                <Code>npm i @sauce-api/visualizer-ui</Code>

                <p>For usage & configuration options, please see <a href="https://www.npmjs.com/package/@sauce-api/visualizer-ui">https://www.npmjs.com/package/@sauce-api/visualizer-ui</a></p>
            </div>
        </div>
    );
};
