import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "daisy-ui-react-components";
import React from "react";
import { Link } from "react-router-dom";
import { Code } from "../../generic/Code";

export const Installation = () => {
    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose max-w-4xl flex-grow">
                <h1>Installing Sauce API</h1>
                <p>We provide a CLI to make provisioning Sauce projects as painless as possible.</p>

                <h3>Installing the CLI</h3>
                <p>Installing via NPM</p>
                <Code>npm install -g @sauce-api/cli</Code>

                <p>Installing via Yarn</p>
                <Code>yarn global add @sauce-api/cli</Code>

                <h3>Creating a new project</h3>
                <p>Once the CLI is installed, you can create a new project by running the below command in your terminal</p>
                <Code>sauce-cli create --name=my-first-sauce-app</Code>

                <p>This will generate a project in a directory named <span className="badge rounded-md">my-first-sauce-app</span> and will have a folder structure of:</p>
                <Code
                    disableCopy={true}
                    className={"w-full"}
                >
                    {
                        `my-first-sauce-app
                        ├── README.md
                        ├── config.json
                        ├── local-config.json
                        ├── package-lock.json
                        ├── package.json
                        ├── src
                        │   ├── @types
                        │   │   └── app-env.d.ts
                        │   ├── api
                        │   │   ├── App.ts
                        │   │   ├── RouteDefinitions.ts
                        │   │   ├── example
                        │   │   │   ├── example.controller.ts
                        │   │   │   ├── example.int-test.ts
                        │   │   │   ├── example.routes.ts
                        │   │   │   └── example.service.ts
                        │   │   ├── root
                        │   │   │   ├── root.controller.ts
                        │   │   │   └── root.routes.ts
                        │   ├── config
                        │   │   ├── ErrorHandler.ts
                        │   │   └── Sauce
                        │   │       ├── PaginationConfig.ts
                        │   │       ├── Policies.ts
                        │   │       ├── RouteTags.ts
                        │   │       └── SauceConfig.ts
                        │   ├── index.ts
                        │   ├── modules
                        │   │   ├── logger
                        │   │   │   ├── bunyanConfig.ts
                        │   │   │   ├── logger.ts
                        │   │   │   └── serializers
                        │   │   │       ├── req.ts
                        │   │   │       └── res.ts
                        │   │   ├── route-params
                        │   │   │   └── CommonParams.ts
                        │   │   └── route-validators.ts
                        │   └── test-utils
                        │       └── testApp.ts
                        └── tsconfig.json`
                    }
                </Code>

                <div className="my-10 flex justify-end">
                    <Link to="/starting-up" className="no-underline text-primary-content">
                        <Button variant="primary" className="rounded-md">
                            Next: Starting things up
                            <FontAwesomeIcon className="ml-2" icon={"arrow-circle-right"} />
                        </Button>
                    </Link>
                </div>
            </div>
        </div>
    );
};
