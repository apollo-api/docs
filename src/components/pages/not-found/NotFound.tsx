import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

export const NotFound = () => {
    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="hero min-h-[75vh]">
                <div className="hero-content text-center">
                    <div className="max-w-md">
                        <h1 className="text-8xl md:text-[10rem] xl:text-[12rem] font-bold">4<FontAwesomeIcon icon={"frown"}/>4</h1>
                        <p className="py-6">We're sorry, we can't find what you're looking for.</p>
                    </div>
                </div>
            </div>
        </div>
    );
};
