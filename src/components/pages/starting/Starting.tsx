import React from "react";
import { Code } from "../../generic/Code";

export const Starting = () => {
    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose w-full max-w-4xl flex-grow">
                <h1>Starting Sauce API</h1>
                <p>Sweet! We have a project now. Let's start it up:</p>

                <p>First let's go into our new app directory:</p>
                <Code>cd ./my-first-sauce-app</Code>

                <p>Next, let's install our node modules:</p>
                <Code>npm install</Code>

                <p>Now we can start our app up with:</p>
                <Code>npm run start-dev</Code>

                <p>
                    The app will start on port 1337 by default. You can see one of the example routes in action
                    by making a GET request to the below url:
                </p>
                <Code>http://localhost:1337/example</Code>
            </div>
        </div>
    );
};
