export const usage = `
/* CONTROLLER ACTION */
public async index() :Promise<void> {
    try{
        return this.responses.responseObject(200, {
            firstName: "John",
            lastName: "Doe",
            age: 30
        });
    } catch(err){
        this.next(err);
    }
}`;

export const returns = `{
    "response": {
      "firstName": "John",
      "lastName": "Doe",
      "age": 30
    },
    "code": 200
}`;
