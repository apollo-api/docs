export default `
/* CONTROLLER ACTION */
public async index() :Promise<void> {
    try{
        return this.responses.responseText(200, "Example response");
    } catch(err){
        this.next(err);
    }
}`;
