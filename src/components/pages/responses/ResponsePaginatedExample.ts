export const usage = `
/* CONTROLLER ACTION */
public async index() :Promise<void> {
    try{
        const data = [
            {
                firstName: "John",
                lastName: "Doe",
                age: 30
            },
            {
                firstName: "Jane",
                lastName: "Doe",
                age: 35
            }
        ];
        return this.responses.responsePaginated(200, this.service.paginate(data));
    } catch(err){
        this.next(err);
    }
}`;

export const returns = `{
    "response": [
        {
            "firstName": "John",
            "lastName": "Doe",
            "age": 30
        },
        {
            "firstName": "Jane",
            "lastName": "Doe",
            "age": 35
        }
    ],
    "page": {
        "next": "https://some-url?pageSize=2&page=2",
        "prev": "",
        "current": 1,
        "size": 2
    }
    "code": 200
}`;
