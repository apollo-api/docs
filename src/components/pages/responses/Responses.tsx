import React from "react";
import { Light as SyntaxHighlighter } from "react-syntax-highlighter";
import ts from "react-syntax-highlighter/dist/esm/languages/hljs/typescript";
import { MethodDoc } from "../../generic/MethodDoc";
SyntaxHighlighter.registerLanguage("typescript", ts);
import { PropTableItem} from "../../generic/Table";
import ResponseTextExample from "./ResponseTextExample";
import * as ResponseObjectExample from "./ResponseObjectExample";
import * as ResponsePaginatedExample from "./ResponsePaginatedExample";
import * as ResponseArrayExample from "./ResponseArrayExample";
import * as ErrorResponseExamples from "./ErrorResponseExamples";
import { Link } from "react-router-dom";

export const Responses = () => {
    const commonResponseArgs :PropTableItem[] = [
        {
            name: "code",
            description: "HTTP Response code",
            dataType: "number",
            required: true
        },
    ];

    const errorResponseArgs :PropTableItem[] = [
        {
            name: "data",
            description: "This is your error message sent back in the response",
            dataType: "any (string recommended)",
            required: true
        }
    ];

    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose max-w-4xl flex-grow">
                <h1>Response Methods</h1>
                <p>
                    See below for all built in response methods. Every response method aside from
                    <span className="font-bold"> responseText </span>
                    will stay consistent with Sauce's response structure
                </p>

                <MethodDoc
                    header="Responses.responseText(code, data)"
                    usageExample={ResponseTextExample}
                    methodArgs={[
                        ...commonResponseArgs,
                        {
                            name: "data",
                            description: "Response content to send",
                            dataType: "string | number",
                            required: true
                        },
                    ]}
                >
                    <p>
                    This is response method is the most unopinionated.
                    It will send back whatever data you supply in whatever way you supply it.
                    You can essentially treat this as the <a href="https://expressjs.com/en/5x/api.html#res.send" target={"_blank"}>
                        Express send()
                        </a> method.
                    </p>
                </MethodDoc>

                <MethodDoc
                    header="Responses.responseObject(code, data)"
                    usageExample={ResponseObjectExample.usage}
                    returnsExample={ResponseObjectExample.returns}
                    methodArgs={[
                        ...commonResponseArgs,
                        {
                            name: "data",
                            description: "Object of response data",
                            dataType: "object",
                            required: true
                        },
                    ]}
                >
                    <p>Sends a JSON response object</p>
                </MethodDoc>

                <MethodDoc
                    header="Responses.responsePaginated(code, data)"
                    usageExample={ResponsePaginatedExample.usage}
                    returnsExample={ResponsePaginatedExample.returns}
                    methodArgs={[
                        ...commonResponseArgs,
                        {
                            name: "data",
                            description: "Object of paginated response data",
                            dataType: "object",
                            required: true
                        },
                    ]}
                >
                    <p>
                        Sends a JSON response object of paginated data. See <Link to="/services">Services</Link> for
                        more details on building the pagination object.
                    </p>
                </MethodDoc>

                <MethodDoc
                    header="Responses.responseArray(code, data)"
                    usageExample={ResponseArrayExample.usage}
                    returnsExample={ResponseArrayExample.returns}
                    methodArgs={[
                        ...commonResponseArgs,
                        {
                            name: "data",
                            description: "Array of response data",
                            dataType: "array",
                            required: true
                        },
                    ]}
                >
                    <p>Sends a JSON response object of containing an array of unpaginated data.</p>
                </MethodDoc>

                <MethodDoc
                    header="Responses.badRequest(data)"
                    usageExample={ErrorResponseExamples.badRequestUsage}
                    returnsExample={ErrorResponseExamples.badRequestReturns}
                    methodArgs={errorResponseArgs}
                >
                    <p>Sends a JSON response object back with an http status code of <span className="font-bold">400</span></p>
                </MethodDoc>

                <MethodDoc
                    header="Responses.serverError(data)"
                    usageExample={ErrorResponseExamples.serverErrorUsage}
                    returnsExample={ErrorResponseExamples.serverErrorReturns}
                    methodArgs={errorResponseArgs}
                >
                    <p>Sends a JSON response object back with an http status code of <span className="font-bold">500</span></p>
                </MethodDoc>

                <MethodDoc
                    header="Responses.notFound(data)"
                    usageExample={ErrorResponseExamples.notFoundUsage}
                    returnsExample={ErrorResponseExamples.notFoundReturns}

                    methodArgs={errorResponseArgs}
                >
                    <p>Sends a JSON response object back with an http status code of <span className="font-bold">404</span></p>
                </MethodDoc>

                <MethodDoc
                    header="Responses.unauthorized(data)"
                    usageExample={ErrorResponseExamples.unauthorizedUsage}
                    returnsExample={ErrorResponseExamples.unauthorizedReturns}
                    methodArgs={errorResponseArgs}
                >
                    <p>Sends a JSON response object back with an http status code of <span className="font-bold">401</span></p>
                </MethodDoc>
            </div>
        </div>
    );
};
