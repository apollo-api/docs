export const usage = `
/* CONTROLLER ACTION */
public async index() :Promise<void> {
    try{
        const data = [
            {
                firstName: "John",
                lastName: "Doe",
                age: 30
            },
            {
                firstName: "Jane",
                lastName: "Doe",
                age: 35
            }
        ];
        return this.responses.responseArray(200, data);
    } catch(err){
        this.next(err);
    }
}`;

export const returns = `{
    "response": [
        {
            "firstName": "John",
            "lastName": "Doe",
            "age": 30
        },
        {
            "firstName": "Jane",
            "lastName": "Doe",
            "age": 35
        }
    ],
    "code": 200
}`;
