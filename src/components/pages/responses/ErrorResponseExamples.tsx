export const badRequestUsage = `
/* CONTROLLER ACTION */
public async index() :Promise<void> {
    try{
        const something :number = <any>this.req.params["something"];
        if(something !== 1){
            return this.responses.badRequest("'something' must equal 1");
        }
    } catch(err){
        this.next(err);
    }
}`;

export const badRequestReturns = `{
    "response": {},
    "code": 400,
    "error": "Bad Request",
    "error_description": "'something' must equal 1"
}`;

export const serverErrorUsage = `
/* CONTROLLER ACTION */
public async index() :Promise<void> {
    try{
        return this.responses.serverError("Something went wrong!");
    } catch(err){
        this.next(err);
    }
}`;

export const serverErrorReturns = `{
    "response": {},
    "code": 500,
    "error": "Internal Server Error",
    "error_description": "Something went wrong!"
}`;

export const notFoundUsage = `
/* CONTROLLER ACTION */
public async index() :Promise<void> {
    try{
        return this.responses.notFound("User not found");
    } catch(err){
        this.next(err);
    }
}`;

export const notFoundReturns = `{
    "response": {},
    "code": 404,
    "error": "Not Found",
    "error_description": "User not found"
}`;

export const unauthorizedUsage = `
/* CONTROLLER ACTION */
public async index() :Promise<void> {
    try{
        return this.responses.unauthorized("You do not have access to this");
    } catch(err){
        this.next(err);
    }
}`;

export const unauthorizedReturns = `{
    "response": {},
    "code": 401,
    "error": "Unauthorized",
    "error_description": "You do not have access to this"
}`;
