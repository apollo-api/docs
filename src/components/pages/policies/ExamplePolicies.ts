export default
`import { formatError } from "@sauce-api/core";
import { isTokenBlackListed, readToken, validateToken } from "../../../modules/Auth/Auth";
import { TokenData } from "../../interfaces/Auth.types";
import { SauceType } from "../SauceConfig";


export interface Policies{
    isAuthenticated: (Sauce :SauceType) => Promise<void>
    isSelf: (Sauce :SauceType) => Promise<void>
    isSelfOrAdmin: (Sauce :SauceType) => Promise<void>
    isAdmin: (Sauce :SauceType) => Promise<void>
}
export type PolicyNames = keyof Policies;

export const PolicyMethods :Policies = {
    /** Request must have a valid authentication token  */
    isAuthenticated: async (Sauce :SauceType) :Promise<void> =>{
        try{
            const token = Sauce.req.get("Authorization");
            if(token) {
                const blackListed = await isTokenBlackListed(token, Sauce.custom.tokenStore);

                if(blackListed && validateToken(token)) {
                    throw formatError(400, "Invalid Token");
                }
            }
            else{
                throw formatError(401, "You're not authorized to make this request");
            }
        }
        catch(err) {
            throw err;
        }
    },

    /** Requesting token must belong to the user the request is being made for */
    isSelf: async (Sauce :SauceType) :Promise<void> =>{
        try{
            const token = readToken(Sauce.req.get("Authorization"));
            const userId :any = parseInt(Sauce.req.params["userId"]);
            if(token.userId !== userId) {
                throw formatError(401, "You're not authorized to make this request");
            }
        }
        catch(err) {
            throw err;
        }
    },

    /** Requesting token must belong to either the user the request is being made
     * for or an admin
    */
    isSelfOrAdmin: async (Sauce :SauceType) :Promise<void> =>{
        try{
            const token = readToken(Sauce.req.get("Authorization"));
            const userId :any = parseInt(Sauce.req.params["userId"]);
            if(token.userId !== userId) {
                if(!token.isAdmin) {
                    throw formatError(401, "You're not authorized to make this request");
                }
            }
        }
        catch(err) {
            throw err;
        }
    },

    /**
     * Requesting token must belong to an admin
    */
    isAdmin: async (Sauce :SauceType) :Promise<void> =>{
        try{
            const token :TokenData = readToken(Sauce.req.get("Authorization"));
            if(!token.isAdmin) {
                throw formatError(401, "You're not authorized to make this request");
            }
        }
        catch(err) {
            throw err;
        }
    },
};`;
