// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Light as SyntaxHighlighter } from "react-syntax-highlighter";
import ts from "react-syntax-highlighter/dist/esm/languages/hljs/typescript";
import atom from "react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark";
SyntaxHighlighter.registerLanguage("typescript", ts);
import { PropTableItem, Table } from "../../generic/Table";
import ExamplePolicies from "./ExamplePolicies";

export const Policies = () => {
    const configProps :PropTableItem[] = [
        {
            name: "Sauce",
            description: `Every route policy gets the Sauce object automatically passed to it as an argument.
            For more details please see: <a href="${process.env.PUBLIC_URL}/sauce-object">Sauce Object</a>`,
            dataType: "Object",
            required: false
        },
    ];

    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose max-w-4xl flex-grow">
                <h1>Policies</h1>
                <p>
                    Route policies are a powerful middleware that you can assign to routes.
                    Very useful for enforcing authentication only routes, routes based on user permissions (like adminOnly), etc.
                    You can even use policies to perform more complex route param validations if the out of the box
                    validaitons don't suit your needs.
                </p>

                <p>Sauce expects you to pass your policies as an object of methods to the config object. See the example below for details</p>

                <h2 className="mb-0">Polciy Arguments</h2>
                <div className="overflow-x-auto">
                    <Table
                        properties={configProps}/>
                </div>
            </div>

            <p>Example Policies object: </p>
            <div className="prose w-full max-w-4xl flex-grow">
                <SyntaxHighlighter
                    language="typescript"
                    className="bg-neutral text-neutral-content m-2 p-3 rounded-md overflow-x-auto"
                    style={atom}
                >
                    {ExamplePolicies}
                </SyntaxHighlighter>
            </div>
        </div>
    );
};
