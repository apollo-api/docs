import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "daisy-ui-react-components";
import React from "react";
import { Link } from "react-router-dom";

export const Home = () => {
    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose max-w-4xl flex-grow">
                <h1>Welcome to Sauce API!</h1>
                <p>Sauce is a framework built on top of <a href="https://expressjs.com/">Express JS</a> and
                adds some useful things right out of the box on top of Express JS's offerings to make creating a
                RESTful API even easier.</p>

                <h2>Why use Sauce?</h2>
                <p>Sauce adds some cool things in addition to Express's functionality. Things like:</p>
                <ul>
                    <li>Robust route building blocks that simplify <Link to="/routes">routing</Link></li>
                    <li><Link to="/route-params">Route params</Link> with built in validations. This includes validating JSON request body payloads.</li>
                    <li>Out of the box <Link to="/policies">route policies</Link></li>
                    <li>Out of the box <Link to="/pagination">pagination</Link></li>
                    <li>Implicit consistency across your app's organization & request <Link to="/responses">response structure</Link></li>
                    <li>Out of the box easily accessible <Link to="/sauce-object">globals</Link> throughout your application</li>
                    <li><Link to="/visualizer-ui">An optional plugin</Link> that automagically generates a UI for your API documentation</li>
                </ul>

                <div className="my-10 flex justify-end">
                    <a href={`${process.env.PUBLIC_URL}/installation`} className="no-underline text-primary-content">
                        <Button variant="primary" className="rounded-md">
                            Install Sauce
                            <FontAwesomeIcon className="ml-2" icon={"arrow-circle-right"} />
                        </Button>
                    </a>
                </div>
            </div>
        </div>
    );
};
