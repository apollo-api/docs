import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Link } from "react-router-dom";
import { Light as SyntaxHighlighter } from "react-syntax-highlighter";
import ts from "react-syntax-highlighter/dist/esm/languages/hljs/typescript";
import atom from "react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark";
SyntaxHighlighter.registerLanguage("typescript", ts);
import { PropTableItem, Table } from "../../generic/Table";
import ExampleObject from "./ExampleObject";

export const SauceObject = () => {
    const configProps :PropTableItem[] = [
        {
            name: "req",
            description: `This is automatically part of the Sauce object. This is the Express request object.
            For more details please see: <a href="https://expressjs.com/en/5x/api.html#req" target="_blank">Request</a>`,
            dataType: "Object"
        },
        {
            name: "res",
            description: `This is automatically part of the Sauce object. This is the Express response object.
            For more details please see: <a href="https://expressjs.com/en/5x/api.html#res" target="_blank">Response</a>`,
            dataType: "Object"
        },
        {
            name: "next",
            description: `This is automatically part of the Sauce object. This is the Express Next method.
            For more details please see: <a href="https://expressjs.com/en/guide/writing-middleware.html" target="_blank">Next</a>`,
            dataType: "Function"
        },
        {
            name: "app",
            description: `This is automatically part of the Sauce object. This is the Express Application object.
            For more details please see: <a href="https://expressjs.com/en/5x/api.html#app" target="_blank">Application</a>`,
            dataType: "Object"
        },
        {
            name: "currentRoute",
            description: `This is automatically part of the Sauce object. This gives you access to the current route's configuration.
            For more details please see: <a href="${process.env.PUBLIC_URL}/routes">Routes</a>`,
            dataType: "Object"
        },
        {
            name: "config",
            description: `This is automatically part of the Sauce object. This gives you access to the Sauce config object.
            For more details please see: <a href="${process.env.PUBLIC_URL}/sauce-config">Sauce Config</a>`,
            dataType: "Object"
        },
        {
            name: "custom",
            description: "The custom object allows you to pass whatever you want into the Sauce object. This will allow you to easily reference custom global properties throughout your entire app.",
            dataType: "Object",
            required: false
        },
    ];

    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose max-w-4xl flex-grow">
                <h1>Sauce Object</h1>
                <div className="not-prose alert max-w-md">
                    <div>
                        <FontAwesomeIcon icon={"exclamation-circle"}/>
                        <span className="text-xs sm:text-base">
                            The Sauce object is built for <span className="font-bold">every</span> incoming request
                        </span>
                    </div>
                </div>
                <p>
                    The Sauce object is built on the fly inside of the <Link to="/routes">bindRoutes</Link> method. It is passed to every Controller & Service throughout Sauce.
                    It's main purpose is to allow for easily accessing global properties (db pools, request & response objects, etc) in any route.
                </p>

                <h2 className="mb-0">Properties</h2>
                <div className="overflow-x-auto">
                    <Table
                        properties={configProps}/>
                </div>
            </div>

            <p>Example Object: </p>
            <div className="prose w-full max-w-4xl flex-grow">
                <SyntaxHighlighter
                    language="typescript"
                    className="bg-neutral text-neutral-content m-2 p-3 rounded-md overflow-x-auto"
                    style={atom}
                >
                    {ExampleObject}
                </SyntaxHighlighter>
            </div>
        </div>
    );
};
