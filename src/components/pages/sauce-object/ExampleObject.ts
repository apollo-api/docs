export default
`type Sauce<custom=ObjectOfAnything> = {
    req :Request,
    res :Response,
    next :NextFunction,
    app :Application,
    currentRoute :Route,
    config :SauceConfig,
    custom ?:custom
}`;
