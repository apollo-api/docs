export default
`import { Sauce, SauceConfig } from "@sauce-api/core";
import * as path from "path";
import * as logger from "../../modules/logger/logger";
import { Policies, PolicyMethods } from "./Routes/Policies";
import { RouteDefinitions } from "./Routes/RouteDefinitions";

const isProd = process.env.NODE_ENV === "production";

// Sauce custom items will be available via the Sauce object in 
// every route. This is the type for the custom object, not the
// custom object itself
export type SauceCustom = {
    something :string
}

// Easily reference Sauce with SauceCustom object by 
// exporting your own SauceType like so:
export type SauceType = Sauce<SauceCustom>;

export const sauceConfig :SauceConfig<SauceCustom, Policies> = {
    routes: RouteDefinitions,
    controllerDirectory: path.resolve(__dirname, "../../api"),
    controllerExtension: isProd ? "js" : "ts",
    policies: PolicyMethods,
    environments: {
        local: {
            url: "http://localhost:1337"
        },
        qa: {
            url: "http://some-qa-site"
        },
        prod: {
            url: "https://some-production-site"
        }
    },
    logger,
    pagination: {
        pageSize: 25,
        max: 100
    }
};`;
