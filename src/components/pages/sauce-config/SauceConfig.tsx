import React from "react";
import { Light as SyntaxHighlighter } from "react-syntax-highlighter";
import ts from "react-syntax-highlighter/dist/esm/languages/hljs/typescript";
import atom from "react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark";
SyntaxHighlighter.registerLanguage("typescript", ts);
import { PropTableItem, Table } from "../../generic/Table";
import ExampleConfig from "./ExampleConfig";

export const SauceConfig = () => {
    const configProps :PropTableItem[] = [
        {
            name: "environments",
            description: "This is mostly used for the built in pagination logic.",
            dataType: "Object",
            required: true
        },
        {
            name: "routes",
            description: `This is where you pass in configured Sauce API routes.
            For more details please see: <a href="${process.env.PUBLIC_URL}/routes">Routes</a>`,
            dataType: "Array",
            required: true
        },
        {
            name: "controllerDirectory",
            description: "The path of the directory your API route controllers live in.",
            dataType: "String",
            required: true
        },
        {
            name: "policies",
            description: "An object of route policy methods.",
            dataType: "Object",
            required: false
        },
        {
            name: "pagination",
            description: "Allows you to customize Sauce's built in pagination functionality. Things like default page size, max page size, etc",
            dataType: "Object",
            required: false
        },
        {
            name: "controllerExtension",
            description: `
                Defaults to
                
                <span class=\"badge badge-ghost m-[1px]\">
                .ts
                </span>
                . For production it's recommended to set this to something along the lines of
                
                <span class=\"badge badge-ghost m-[1px]\">
                    controllerExtension: isProd ? 'js' : 'ts'
                </span>
            `,
            dataType: "Object",
            required: false
        },
        {
            name: "disableLogs",
            description: "Disable all logs that stem from the Sauce Core package itself. Logs are enabled by default.",
            dataType: "boolean",
            required: false
        },
        {
            name: "logger",
            description: "Pass in custom logger methods. Can be used with something like Bunyan or Winston if you want built-in Sauce logs to be streamed with that rather than stdout",
            dataType: "Object",
            required: false
        }
    ];

    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose max-w-4xl flex-grow">
                <h1>Sauce Config</h1>
                <p>While it doesn't have to be located here, by default the config is located at <span className="badge">/src/config/Sauce/SauceConfig.ts</span> in an Sauce CLI generated project</p>

                <h2 className="mb-0">Properties</h2>
                <div className="overflow-x-auto">
                    <Table
                        properties={configProps}/>
                </div>
            </div>

            <p>Example Config: </p>
            <div className="prose max-w-4xl flex-grow">

                <SyntaxHighlighter
                    language="typescript"
                    className="bg-neutral text-neutral-content m-2 p-3 rounded-md overflow-x-auto"
                    style={atom}
                >
                    {ExampleConfig}
                </SyntaxHighlighter>
            </div>
        </div>
    );
};
