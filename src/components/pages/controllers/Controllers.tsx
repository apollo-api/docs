// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Link } from "react-router-dom";
import { Light as SyntaxHighlighter } from "react-syntax-highlighter";
import ts from "react-syntax-highlighter/dist/esm/languages/hljs/typescript";
import atom from "react-syntax-highlighter/dist/esm/styles/hljs/atom-one-dark";
SyntaxHighlighter.registerLanguage("typescript", ts);
import { PropTableItem, Table } from "../../generic/Table";
import ExampleController from "./ExampleController";

export const Controllers = () => {
    const configProps :PropTableItem[] = [
        {
            name: "req",
            description: `Automatically set from the Sauce Object. This is the Express request object.
            For more details please see: <a href="${process.env.PUBLIC_URL}/sauce-object">Sauce Object</a>`,
            dataType: "Object"
        },
        {
            name: "res",
            description: `Automatically set from the Sauce Object. This is the Express response object.
            For more details please see: <a href="${process.env.PUBLIC_URL}/sauce-object">Sauce Object</a>`,
            dataType: "Object"
        },
        {
            name: "next",
            description: `Automatically set from the Sauce Object. This is the Express Next method.
            For more details please see: <a href="${process.env.PUBLIC_URL}/sauce-object">Sauce Object</a>`,
            dataType: "Function"
        },
        {
            name: "app",
            description: `Automatically set from the Sauce Object. This is the Express Application object.
            For more details please see: <a href="${process.env.PUBLIC_URL}/sauce-object">Sauce Object</a>`,
            dataType: "Object"
        },
        {
            name: "currentRoute",
            description: `Automatically set from the Sauce Object. This gives you access to the current route's configuration.
            For more details please see: <a href="${process.env.PUBLIC_URL}/routes">Routes</a>`,
            dataType: "Object"
        },
        {
            name: "config",
            description: `Automatically set from the Sauce Object. This gives you access to the Sauce config object.
            For more details please see: <a href="${process.env.PUBLIC_URL}/sauce-config">Sauce Config</a>`,
            dataType: "Object"
        },
        {
            name: "responses",
            description: `A built in class of response handlers.
            For more details please see: <a href="${process.env.PUBLIC_URL}/responses">Responses</a>`,
            dataType: "Object"
        },
    ];

    return (
        <div className="px-5 pt-5 bg-base-100 w-100 min-h-screen">
            <div className="prose max-w-4xl flex-grow">
                <h1>Controllers</h1>
                <p>
                    Controllers are the brain of your routes. Every route must have a controller & a controller action. This is where your
                    controller actions live for their respective route(s).
                </p>
                <h2>File Naming Convention</h2>
                <p>
                    By default, Sauce expects a specific naming convention for your controllers. For example, let's say you created a route
                    and specified a controller of <span className="badge badge-small badge-ghost">example</span> for a route
                    of <span className="badge badge-small badge-ghost">GET /some-route</span>. When that route gets hit, Sauce will look in
                    the configured controller directory (see <Link to={"/sauce-config"} className="text-sm">Sauce Config</Link>) for a controller
                    named <span className="badge badge-small badge-ghost">example.controller.&#60;CONFIGURED EXTENSION&#62; </span> (based on your configured file extension,
                    again see <Link to={"/sauce-config"} className="text-sm">Sauce Config</Link>)
                </p>
                <p>
                    However you can override that naming convention by specify a specific controller
                    with <span className="badge badge-small badge-ghost">customControllerPath</span> in your route
                    (see <Link to={"/routes"} className="text-sm">Routes</Link>)
                </p>

                <h2 className="mb-0">Properties</h2>
                <div className="overflow-x-auto">
                    <Table
                        properties={configProps}/>
                </div>
            </div>

            <p>Example Controller: </p>
            <div className="prose w-full max-w-4xl flex-grow">
                <SyntaxHighlighter
                    language="typescript"
                    className="bg-neutral text-neutral-content m-2 p-3 rounded-md overflow-x-auto"
                    style={atom}
                >
                    {ExampleController}
                </SyntaxHighlighter>
            </div>
        </div>
    );
};
