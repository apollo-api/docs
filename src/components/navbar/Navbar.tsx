import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { MenuLink } from "../generic/MenuLink";
import { Collapse, CollapseBody, CollapseTitle, Menu, MenuItem } from "daisy-ui-react-components";

export function Navbar() {
    const activePage = location.pathname;
    const gettingStartedRoutes = [
        {
            path: "/installation",
            title: "Step 1: Installation"
        },
        {
            path: "/starting-up",
            title: "Step 2: Starting Up"
        },
    ];

    const expandGettingStarted = !!gettingStartedRoutes.filter(r=> r.path.includes(activePage) && activePage !== "/").length;

    const menuLinkClickHandler = (event :React.MouseEvent<HTMLLIElement | HTMLAnchorElement>) =>{
        const drawer = document.getElementById("main-drawer") as HTMLInputElement;
        if(drawer.checked) {
            drawer.checked = false;
        }
    };

    return (
        <div className="bg-base-200 w-80 h-[100vh] overflow-y-auto px-2">
            <div className="z-20 bg-base-200 bg-opacity-90 backdrop-blur sticky top-0 items-center pt-2">
                <img src="https://gitlab.com/sauce-api/core/-/raw/master/sauce-logo.png" width="50%" className="mx-auto hidden lg:block border-4 rounded-full border-primary" />

                <div className="gap-2 px-4 py-5 hidden lg:flex">
                    <div className="logo-font font-title text-primary inline-flex text-2xl transition-all duration-200 md:text-4xl">
                        <span className="text-base-content mr-2">Sauce</span>
                        <span>Api</span>
                    </div>
                    <div className="ml-2 text-sm self-center">
                        <a href="https://gitlab.com/sauce-api/core/-/blob/master/CHANGELOG.md" target="_blank" className="badge badge-primary">
                            1.0.3
                        </a>
                    </div>
                </div>
            </div>

            <Menu variant="base-200">
                <MenuLink onClick={menuLinkClickHandler} to="/">
                    <FontAwesomeIcon icon={"house"} className="min-w-[18px] mr-2"/> Home
                </MenuLink>

                <Collapse defaultExpand={expandGettingStarted}>
                    <CollapseTitle className="gap-x-0">
                        <FontAwesomeIcon className="min-w-[18px] mr-2" icon={"flag-checkered"} />
                        <h2 className="w-full">Getting Started</h2>
                    </CollapseTitle>

                    <CollapseBody className="bg-base-200">
                        <div className="pl-7">
                            {
                                gettingStartedRoutes.map((route, i) =>
                                    <MenuLink onClick={menuLinkClickHandler} key={i} to={route.path} className="capitalize">{route.title}</MenuLink>
                                )
                            }
                        </div>
                    </CollapseBody>
                </Collapse>
                <li></li>

                <MenuItem isTitle>Configuration</MenuItem>
                <MenuLink onClick={menuLinkClickHandler} to="/sauce-config">Sauce Config</MenuLink>
                <MenuLink onClick={menuLinkClickHandler} to="/sauce-object">Sauce Object</MenuLink>

                <MenuItem isTitle>Routing</MenuItem>
                <MenuLink onClick={menuLinkClickHandler} to="/controllers">Controllers</MenuLink>
                <MenuLink isDisabled onClick={menuLinkClickHandler} to="/pagination">Pagination</MenuLink>
                <MenuLink onClick={menuLinkClickHandler} to="/policies">Policies</MenuLink>
                <MenuLink onClick={menuLinkClickHandler} to="/responses">Responses</MenuLink>
                <MenuLink isDisabled onClick={menuLinkClickHandler} to="/routes">Routes</MenuLink>
                <MenuLink onClick={menuLinkClickHandler} to="/route-params">Route Params</MenuLink>
                <MenuLink isDisabled onClick={menuLinkClickHandler} to="/services">Services</MenuLink>

                <MenuItem isTitle>Plugins</MenuItem>
                <MenuLink isDisabled onClick={menuLinkClickHandler} to="/minerva">Minerva</MenuLink>
                <MenuLink to="/visualizer-ui">Visualizer UI</MenuLink>
            </Menu>

            <div className="from-base-200 pointer-events-none sticky bottom-0 flex h-20 bg-gradient-to-t to-transparent"></div>
        </div>
    );
}
